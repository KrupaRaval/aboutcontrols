package aboutcontrols.aboutcontrols;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends AppCompatActivity {

    //initialization
    EditText editText,editText1;
    Button btnsub;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //declairation
        editText = (EditText)findViewById(R.id.Text_pass);
        editText1 =(EditText)findViewById(R.id.editText_confpass);
        btnsub =(Button)findViewById(R.id.btn_confirm);

        //method od button onclick
        btnsub.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,"You have submited you data",Toast.LENGTH_SHORT).show();
            }
        });
    }
}